#include <stdio.h>
#include <wiringPi.h>

//Pin number declerations. We're using the Broadcom chip pin numbers.
typedef enum{
	Idle_state,
	ThrowThermistor_state,
	ThrowCarbon_state,
	startShutdown_state
}eSystemState;
//lights
const int ledGreen = 17; // Regular LED-Broadcom pin 11, P1 pin 17
const int ledYellow = 16; // Regular LED-Broadcom pin 36, P1 pin 16
//buttons
const int buttonBlue = 27; 	// Active-low button - Broadcom pin 27, P1 pin 13
const int buttonRed = 22; 	// Active-low button - Broadcom pin 22, P1 pin 15
const int buttonPurple = 18; 	// Active-low button - Broadcom pin 18, P1 pin 12


// Prototypes
eSystemState CheckThermistors(void);
eSystemState CheckCarbonMonoxide(void);

int main(void)
{
  // Setup stuff:
  wiringPiSetupGpio(); // Initialize wiringPi -- using Broadcom pin numbers
  pinMode(ledGreen, OUTPUT); 	// Set green LED as output
  pinMode(ledYellow, OUTPUT); 	// Set green LED as output

  pinMode(buttonBlue, INPUT); 	// Set button as INPUT
  pinMode(buttonRed, INPUT); 	// Set button as INPUT
  pinMode(buttonPurple, INPUT); // Set button as INPUT
  
  digitalWrite(ledYellow, LOW);    // Turn off Yellow LED
  digitalWrite(ledGreen, LOW);    // Turn off Green LED
  
  eSystemState nextState = Idle_state;

  while(1)
  {
    switch(nextState){
      case Idle_state:
        nextState = CheckThermistors();
        break;
      case ThrowThermistor_state:
	nextState = CheckCarbonMonoxide();
	break;
      case ThrowCarbon_state:
	//nextState = ShutOff();
	break;
      default:
        nextState = Idle_state;
	break;
    }
    delay(100);
  }
  return 0;
}
eSystemState CheckThermistors(void){
  int throwFlag;
  if(digitalRead(buttonBlue) || digitalRead(buttonRed)){
    throwFlag = 1;
    digitalWrite(ledGreen, HIGH);    // Turn on Green LED

  }
  else{
    throwFlag = 0;
    digitalWrite(ledGreen, LOW);    // Turn off Green LED
  }
  if(throwFlag == 1){
    printf("WARNING: There is a spike in temperature");
    return ThrowThermistor_state;
  }
  return Idle_state;
}
eSystemState CheckCarbonMonoxide(void){
  int throwFlag;
  if(digitalRead(buttonPurple)){
    throwFlag = 1;
    digitalWrite(ledYellow, HIGH);    // Turn on Yellow LED
  }
  else{
    throwFlag = 0;
    digitalWrite(ledYellow, LOW);    // Turn off Yellow LED
  }
  if(throwFlag == 1){
    printf("WARNING: Carbon Monoxide present");
    return ThrowCarbon_state;
  }
  return Idle_state;
}
	
	
