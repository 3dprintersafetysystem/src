//Gabriel Ortega
//Omar Corres Salazar

#include <stdio.h>
#include <wiringPi.h>;

const int fanPin=17;
const int buttonPin=27;
const int servoPin=18;


typedef enum {

	//Idle state
	
	//Throw Thermistor flag state
	
	//Throw Carbon Flag state
	
	//Start Shutdown Process State (Ends the program)

} eSystemState


/* Function will check Thermistors 
** if no spike in temp then return idle state no flag needs to be thrown/turn off flag if on
** else return Throw Thermistor flag state throw the flag
*/
eSystemState CheckThermistors(void){
	//If the button is pressed for T1 or T2
	int throwFlag = 1;
	//else
	throwFlag = 0;
	
	if (throwFlag == 1){
		printf("WARNING: There is a spike in temperature");
		return Thermistor_Flag;
	} else {
		return Check_Thermistor;
	}
}

/* Function will check carbon monoxide level
** if no flag needs to be thrown return Check Thermistor state state turn off flag
** else return Shut down process state display the flag
*/
eSystemState CheckCarbonMonoxide(void){

}

/* Turn the camera live feed on
** Turn off all air supply
*/
 ShutDownProcess(void){
 
	printf(“Emergecy Flag set TRUE, enabling fire suppression system.\n”); //Prints alert to screen

	while(1)
	{	
		If(flag = 1){
			digitalWrite(fanPin,HIGH);
		    pwmWrite(servoPin,150);
		    delay(500);
 	      	pwmWrite(servoPin,1000);
			delay(5000);
		else
			digitalWrite(fanPin, LOW);
	}
	return 0;

}


//main function
int main() {

	//Assign a pin from rpi to Thermistor 1
	
	//Assign a pin from rpi to Thermistor 2
	
	//Assign a pin from rpi to Carbon Monoxide Sensor
	
	//sets up the pin modes for the shutdown process as well as pwm param
	wiringPiSetupGpio();
	pinMode(fanPin, OUTPUT);
	pinMode(buttonPin, INPUT);
	pinMode(servoPin, PWM_OUTPUT);
	pwmSetMode(PWM_MODE_MS);
    pwmSetRange (2000);
    pwmSetClock (192);
	
	int flag=0;		//flag set to off

	
	eSystem nextState = Idle_State;
	
	while (1) {
	
		switch(nextState) {
		
		//May not need
		/*case Idle_State:
			nextState = CheckThermistors();//Check the Thermistors for spike in temp
			break;*/
			
		case Check_Thermistor:
			nextState = CheckThermistors();//Check the Thermistors for spike in temp
			break;
			
		case Thermistor_Flag:
			nextState = CheckCarbonMonoxide();//Check the carbon monoxide sensor
			break;
			
		case CarbonMonoxide_Flag:
			ShutdownProcess();
		
		}
	
	}

}