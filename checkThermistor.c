//Gabriel Ortega
#include <stdio.h>
#include <wiringPi.h>
#include <stdlib.h>

const int buttLed1 = 17;
const int buttLed2 = 27;
const int button1Pin = 4;
const int warningLed = 9;
const int button2Pin = 18;

int main(void){

	wiringPiSetupGpio();
	pinMode(buttLed1, OUTPUT);
	pinMode(buttLed2, OUTPUT);
	pinMode(warningLed, OUTPUT);
	pinMode(button1Pin, INPUT);
	pinMode(button2Pin,INPUT);

	int throwFlag1=0; //If thermistor 1 has a spike in temp throw flag(1)
	int throwFlag2=0; //If thermistor 2 has a spike in temp throw flag(1)

	printf("The program is running\n\nPress CNTRL+C to close\n");


	while(1){


		if(digitalRead(button1Pin)){ //Check if temp is spiked at thermistor 1
		
			digitalWrite(buttLed1, HIGH);
			throwFlag1 = 1;

		} else {
			
			digitalWrite(buttLed1, LOW);
			throwFlag1 = 0; 
		}

		if(digitalRead(button2Pin)){ //Checks if themp is spiked at thermistor 1
			
			digitalWrite(buttLed2, HIGH);
			throwFlag2 = 1;
		} else {
			
			digitalWrite(buttLed2, LOW);
			throwFlag2 = 0;
		}


		if (throwFlag1 == 1 || throwFlag2 == 1){

			digitalWrite(warningLed, HIGH);
			printf("WARNING: THERE IS A SPIKE IN TEMPERATURE\n");
			
		} else{

			digitalWrite(warningLed, LOW);
		}
	}
}
